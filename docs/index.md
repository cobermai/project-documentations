# TE-MPE-CB: Reliability, Availability and Machine Learning Project Documentation

This site contains the documentation for reliability, availability and 
machine learning projects in the TE-MPE-CB section.


- [1. Getting Started](#1-getting-started)
  - [1.1. Computer setup](#11-computer-setup)
    - [1.1.1. Logging in on the desktop](#111-logging-in-on-the-desktop)
    - [1.1.2. Installing programs](#112-installing-programs)
      - [1.1.2.1. Installing python](#1121-installing-python)
      - [1.1.2.2 Installing pycharm](#1122-installing-pycharm)
      - [1.1.2.3. Installing mattermost](#1123-installing-mattermost)
  - [1.2. Repository setup](#12-repository-setup)
    - [1.2.1. Cloning the mlframework repo](#121-cloning-the-mlframework-repo)
      - [1.2.1.1. Using http link](#1211-using-http-link)
      - [1.2.1.2. Using ssh keypair](#1212-using-ssh-keypair)
      - [1.2.1.3. Setting up repo for use](#1213-setting-up-repo-for-use)
      - [1.2.1.4. Add printer on Linux](#1214-add-printer-on-linux)
- [2. Execute notebooks with SWAN:](#2-execute-notebooks-with-swan)
  - [2.1. Execute notebooks with SWAN using a GPU from outside CERN:](#21-execute-notebooks-with-swan-using-a-gpu-from-outside-cern)
  - [2.2.  Install packages in SWAN:](#22--install-packages-in-swan)
- [3. How to connect to CERN from outside](#3-how-to-connect-to-cern-from-outside)
  - [3.1.  Remote desktop to CERN](#31--remote-desktop-to-cern)
  - [3.2. Conect your Browser with SSH Proxy](#32-conect-your-browser-with-ssh-proxy)
- [4. How to Submit Jobs to HTCondor](#4-how-to-submit-jobs-to-htcondor)
  - [4.1. Install package to environment](#41-install-package-to-environment)
- [5. How to Set Up Pycharm](#5-how-to-set-up-pycharm)
- [6. How to use Openstack](#6-how-to-use-openstack)
- [7. Python Style Guideline](#7-python-style-guideline)
- [Website Information](#website-information)

## 1. Getting Started
Here are some helpful documentations to getting started.

### 1.1. Computer setup
First task is to ensure your work computer is working and has the needed software installed. 
Although this might sound simple, it can be problematic as there are many caveats that you might run into. 

#### 1.1.1. Logging in on the desktop
When you arrive at your office you can use your CERN username and password to login to the computer.

If the login does not work, write a mail to service desk. The people at CERN service desk are happy to help you with any problems you might experience.

The service desk people will probably tell you to first go to <https://resources.web.cern.ch/resources/>.
Here you click list services, then LXPLUS and Linux, and then subscribe to AFS. 
Now you might need to wait for approx. 2 hours to ensure the user space has been created.

If that does not work, login as root user (ask service desk for info on this) and run ```addusercern username``` in terminal.

Now you should be able to login.

Before going any further, check that you have administrator rights. 
If not, login as root again and change your account status to administrator. 

#### 1.1.2. Installing programs
When installing programs, be sure not to use up your user home folder storage (it has only 2 GB of storage!!!). 
If you use up this storage you will not be able to download or install anything.
Instead, you might use your workspace folder or local home folder to download and install programs.

To check your AFS workspace status go to <https://resources.web.cern.ch/resources/>, click AFS workspace and go to settings. 
Here you can also bump your home folder storage to 5 GB or 10 GB if you accidentally used the initial 2 GB. 

For some programs the easiest method for installing is to login as root user, as you will not have to deal with access right problems then. 

##### 1.1.2.1. Installing python
On centos8 ```sudo yum install python38``` in terminal is the easiest way to install a recent version of python.

Alternatively search for "centos python 3.x install guide". 
You should then find a guide to get the desired version.
                          
##### 1.1.2.2 Installing pycharm
Pycharm is the IDE the team is using.
Go to <https://jetbrains.com/toolbox-app> and install the jetbrains toolbar.
From there install pycharm professional. 

##### 1.1.2.3. Installing mattermost
Mattermost is the communication platform for the team. It is accessible through a web browser at <https://mattermost.web.cern.ch>.
However, if you would like to install it on your work computer as an application, please let us know if you figure out how to.

##### 1.1.2.4 Add printer on Linux
Normaly all printers should already be added and can be directly selected.
1. Search for [printer](https://printservice.web.cern.ch/printservice/UserTools/PrinterStatus) (e.g. 30-5201-CANC3)
2. Download script e.g. 30-5201-CANC3_installPrinter.sh
3. Go to download folder in terminal
4. chmod +x 30-5201-CANC3_installPrinter.sh
5. ./30-5201-CANC3_installPrinter.sh

### 1.2. Repository setup
The following describes how to get ready to work with the mlframework repo.

#### 1.2.1. Cloning the mlframework repo
First we need to clone the repo.
This can be done in a number of ways
You can either use the pycharm tools, use the terminal or any other method that you find fitting for cloning git repos.

##### 1.2.1.1. Using http link
1. On GitLab create an access token.
2. Clone project using https link from GitLab.
3. Use your CERN username as username and use the access token as password.

##### 1.2.1.2. Using ssh keypair
1. Check if you have an ssh keypair. If not, create keypair following the [guide on GitLab](https://gitlab.cern.ch/help/ssh/README.md).
2. Add public key to SSH keys in GitLab preferences. 
3. Clone project using ssh link.

##### 1.2.1.3. Setting up repo for use
In order to be able to work with the repo we need some additional software for linting, testing etc.

The following assumes that your computer runs CentOS 8:
* In the terminal install h5py using ```sudo yum install hdf5``` 
* Install pylint using ```sudo pip install pylint```
* Install mypy using ```sudo pip install mypy```
* Install required packages from ```requirements.txt``` 
* Set the python path by running ```export PYTHONPATH=.```

## 2. Execute notebooks with SWAN:
1. Go to SWAN [https://swan.cern.ch](https://swan.cern.ch) 
(In case you haven’t activated CERNbox yet, first go to [https://cernbox.cern.ch](https://cernbox.cern.ch)  )
2. Clone a git repository https://gitlab.cern.ch/clicml.git into the "SWAN_projects" of your cernbox
3. Execute the chosen notebook, cell by cell

### 2.1. Execute notebooks with SWAN using a GPU from outside CERN:
Please find below the instructions to use the SWAN instance quipped with GPUs:
1a. Connect to [https://swan-k8s.cern.ch](https://swan-k8s.cern.ch) with your CERN username and password
1b. The GPU SWAN instance at https://swan-k8s.cern.ch is still not accessible from outside the CERN network. 
2. In the web form, start your session with "Cuda 10 Python3" as Software stack.
 
3. Once you are in your SWAN session, you can create projects and notebooks as usual in SWAN. The difference is that there are some libraries in your environment that have been compiled with GPU support. For example, if you use TensorFlow from a Python notebook, it will offload computations to the GPU. You also have the nvcc compiler available which you can use from the SWAN terminal.

### 2.2.  Install packages in SWAN:
A typical case is the installation of Python packages, which requires to run pip from a SWAN terminal:
* pip install --user package_name

If this fails because you are trying to install an updated version of a package that already exists in CVMFS, you will need to add the --upgrade flag.
Then, it would be necessary to add the local installation path to PYTHONPATH, by creating a bash startup script that configures that variable (don't forget to call this startup script in the session configuration menu)(use current python version in script):
* export PYTHONPATH=$CERNBOX_HOME/.local/lib/python3.5/site-packages:$PYTHONPATH

As a result of performing the aforementioned steps, the package will be installed on your CERNBox and it will be picked by any notebook you open after that. Since the package is on your CERNBox, it will be also available in any new session you start in SWAN.

## 3. How to connect to CERN from outside
### 3.1.  Remote desktop to CERN
1.	Open windows remote
2.	cernts.cern.ch
3.	Log into CERN\username

### 3.2. Connect your Browser with SSH Proxy
1. In a shell, open an SSH tunnel: ssh -D6789 username@lxplus.cern.ch
2. In Firefox: Preferences->advanced->network->settings. Tick the radio button "Manual Proxy Configuration", SOCKS Host set to 127.0.0.1 and Port to 6789
 
## 4. How to Submit Jobs to HTCondor
Guide: [https://batchdocs.web.cern.ch/local/quick.html](https://batchdocs.web.cern.ch/local/quick.html)

Example submission file:

```python
executable            = htc_run.sh
arguments             = $(ClusterId) $(ProcId) $(settings)
output                = output/htc_run.$(ClusterId).$(ProcId).out
error                 = error/htc_run.$(ClusterId).$(ProcId).err
log                   = log/htc_run.$(ClusterId).log
request_CPUs          = 2
request_GPUs          = 1
requirements          = regexp("V100", TARGET.CUDADeviceName)
+JobFlavour           = "workday"
+AccountingGroup      = "group_u_TE.mpe"
queue 6
```

Example shell script:

```python
echo "activate environment"
python -m virtualenv myvenv
source /afs/cern.ch/user/c/cobermai/Desktop/afs_work/miniconda3/bin/activate base

echo "start script"
python /afs/cern.ch/user/c/cobermai/Desktop/afs_work/PycharmProjects/quench-prediction/src/main.py --output=$3 --settings=$3.json --ProcId=$2
```

### 4.1. Install package to environment
1. In a shell, open an SSH tunnel: ssh -D6789 username@lxplus.cern.ch
2. cd /afs/cern.ch/user/c/cobermai/Desktop/afs_work/miniconda3/bin
3. source activate base
4. conda install -c conda-forge packagename

## 5. How to Set Up Pycharm
Description missing

## 6. How to use Openstack 
Guide: [https://clouddocs.web.cern.ch/gpu/README.html ](https://clouddocs.web.cern.ch/gpu/README.html)

Connect to [instance](https://openstack.cern.ch/project/instances/) via remote desktop

Open new openstack instance:
1.	[https://openstack.cern.ch/project/instances/](https://openstack.cern.ch/project/instances/)
2.	New instance
3.	landb-mainuser: cobermai
4.	landb-responsible: machine-protection-studies

## 7. Python Style Guideline
In order to make our code comparable, please write it like this:
```python
#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.
This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.
Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py
Seriously, use flake8. Atom.io with https://atom.io/packages/linter-flake8
is awesome!
See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
"""


import os  # STD lib imports first
import sys  # alphabetical

import some_third_party_lib  # 3rd party stuff next
import some_third_party_other_lib  # alphabetical

import local_stuff  # local stuff last
import more_local_stuff
import dont_import_two, modules_in_one_line  # IMPORTANT!
from pyflakes_cannot_handle import *  # and there are other reasons it should be avoided # noqa
# Using # noqa in the line above avoids flake8 warnings about line length!


_a_global_var = 2  # so it won't get imported by 'from foo import *'
_b_global_var = 3

A_CONSTANT = 'ugh.'


# 2 empty lines between top-level funcs + classes
def naming_convention():
    """Write docstrings for ALL public classes, funcs and methods.
    Functions use snake_case.
    """
    if x == 4:  # x is blue <== USEFUL 1-liner comment (2 spaces before #)
        x, y = y, x  # inverse x and y <== USELESS COMMENT (1 space after #)
    c = (a + b) * (a - b)  # operator spacing should improve readability.
    dict['key'] = dict[0] = {'x': 2, 'cat': 'not a dog'}


class NamingConvention(object):
    """First line of a docstring is short and next to the quotes.
    Class and exception names are CapWords.
    Closing quotes are on their own line
    """

    a = 2
    b = 4
    _internal_variable = 3
    class_ = 'foo'  # trailing underscore to avoid conflict with builtin

    # this will trigger name mangling to further discourage use from outside
    # this is also very useful if you intend your class to be subclassed, and
    # the children might also use the same var name for something else; e.g.
    # for simple variables like 'a' above. Name mangling will ensure that
    # *your* a and the children's a will not collide.
    __internal_var = 4

    # NEVER use double leading and trailing underscores for your own names
    __nooooooodontdoit__ = 0

    # don't call anything (because some fonts are hard to distiguish):
    l = 1
    O = 2
    I = 3

    # some examples of how to wrap code to conform to 79-columns limit:
    def __init__(self, width, height,
                 color='black', emphasis=None, highlight=0):
        if width == 0 and height == 0 and \
           color == 'red' and emphasis == 'strong' or \
           highlight > 100:
            raise ValueError('sorry, you lose')
        if width == 0 and height == 0 and (color == 'red' or
                                           emphasis is None):
            raise ValueError("I don't think so -- values are %s, %s" %
                             (width, height))
        Blob.__init__(self, width, height,
                      color, emphasis, highlight)

    # empty lines within method to enhance readability; no set rule
    short_foo_dict = {'loooooooooooooooooooong_element_name': 'cat',
                      'other_element': 'dog'}

    long_foo_dict_with_many_elements = {
        'foo': 'cat',
        'bar': 'dog'
    }

    # 1 empty line between in-class def'ns
    def foo_method(self, x, y=None):
        """Method and function names are lower_case_with_underscores.
        Always use self as first arg.
        """
        pass

    @classmethod
    def bar(cls):
        """Use cls!"""
        pass

# a 79-char ruler:
# 34567891123456789212345678931234567894123456789512345678961234567897123456789

"""
Common naming convention names:
snake_case
MACRO_CASE
camelCase
CapWords
"""

# Newline at end of file
```


## Website Information
The source files for this website can be found in this [GitLab repository](https://gitlab.cern.ch/cobermai/project-documentations/-/tree/master).

Click [here](https://how-to.docs.cern.ch/new/) if you want to create your own
Markdown-based static documentation site with MkDocs from scratch.
