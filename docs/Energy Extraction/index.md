# Energy Extraction

This project is a reliability study of the HL-LHC Energy Extraction systems. The upgraded version involves several new concepts and a new technology that has not been used earlier at CERN (vacuum interrupters). The objective of this project is to quantify the likelihood of critical failures through modelling and simulation methods. Any period of EE system malfunctioning might expose the corresponding magnet to potentially severe outcomes of quenches, rendering it vulnerable to irreversible damage. Such damage might cause long delays of a 1-month to a 1-year interval in the LHC risk matrix. High, near-perfect reliability whenever quench occurs is therefore very important to ensure an appropriate level of protection and to meet the adopted targets. Simulations performed within this study include investigation of the failure rates estimations for individual components, redundant aspects of the new system, planned monitoring and maintenance strategies. The models are developed and evaluated in AvailSim4 - a simulation software developed in-house specifically to address the needs of availability and reliability studies in accelerator-related systems.

![HL-LHC Energy Extraction system schema](images/ees_schema.png)

## Minutes

### 2020
* [21-10-2021: 1st meeting of the Energy Extraction Reliability Study](minutes/EE_reliability_1_201021.docx)
    - [outline of the schema](minutes/EE_reliability_1_201021.pptx)
* [29-10-2021: 2nd meeting of the Energy Extraction Reliability Study](minutes/EE_reliability_2_201029.docx)
    - [general diagram](minutes/EE_reliability_2_201029.odp)
* [E-mail message](minutes/EE_reliability_SM18_study.docx)

### 2021
* [11-02-2021: Meeting regarding access to historical EE systems data](minutes/EE_historicaldata_210211_TC.docx)
* [15-02-2021: Meeting - practical aspects of querying and accessing historical EE systems data](minutes/EE_historicaldata_210215_TC.docx)
* [02-03-2021: Meeting with B. Panev](minutes/EE_reliability_3_210302_TC.docx)
* [19-03-2021: Meeting with B. Panev](minutes/EE_reliability_4_210319_TC.docx)
    - [presentation](minutes/EE_reliability_4_210319_presentation_final.pptx)
* [28-04-2021: Meeting with B. Panev](minutes/EE_reliability_5_210428_TC.docx)
    - [presentation](minutes/EE_reliability_5_210428_presentation.pptx)
* [31-05-2021: Meeting with B. Panev](minutes/EE_reliability_6_210531_TC.docx)
    - [presentation](minutes/EE_reliability_6_210531_presentation.pptx)
* [21-05-2021: Presentation (progress overview)](minutes/EE_presentation_7_210523.pdf)
* [13-07-2021: Meeting with B. Panev](minutes/EE_reliability_8_210713.docx)
    - [presentation](minutes/EE_reliability_8_210713_presentation.pptx)
* [20-08-2021: Meeting with B. Panev](minutes/EE_reliability_9_210820_TC.docx)