# Quench Prediction

Machine learning study for quench prediction. 
We analyze data from 9 different quenches of the FRESCA2c assembly.
This data includes the triggered PM file around the quench, with all voltage taps and quench antenna signals.
Additionally, the recording of the full ramp at slightly lower frequency (10 kHz) is given.

All our code is available in this
[GitLab repository](https://gitlab.cern.ch/cobermai/qench-prediction).

## Minutes

### 2020
* [20201203: Kick off meeting](minutes/20201203.txt)
### 2021
* [20210202: Signal explanation](minutes/20210202_Signal_explanation.txt)
* [20210225: First results](minutes/20210225_first_results.txt)
