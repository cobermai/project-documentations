************************************************************************************************************************
************************************************** MINUTES *************************************************************
************************************************************************************************************************


************************************************************************************************************************
************************************************** EMAILS **************************************************************
************************************************************************************************************************

Hi Nuria
I was wondering if it would be possible to run an experiment in XB3 before we remove the structures which are currently
installed? It's to get some supplementary data for the machine learning study,
I think it would probably take about one week.
We don't have to modify the acquisition system or the software or anything like that.
Basically what I'd like to do is run at a (slightly reduced) fixed input power for a few days and set the vacuum
interlock threshold very low.

The idea is to see how the breakdowns correlate with vacuum interlocks (i.e. if the system interlocks before a breakdown
occurs and to see if breakdowns often occur shortly after vacuum spikes/interlocks).
I can arrange a meeting to discuss/explain in more detail if you'd like.

Regards
Lee
************************************************************************************************************************

Hi Lee,
I see a couple of problems to what you suggest
The experiment is not parasitic. A week is a lot of time for the boxes and I am not sure we will have finished with the
BDR data needed that we have not defined yet.
I you lower the power and the vacuum threshold you will be interlocking for vacuum most if not all the time and
not having any breakdowns.

If I understood well he last meeting, we still do not know if the timing of the vacuum signals and the RF signals can be
synchronized. From the last meeting, I thought the most useful thing will be to change the rate at which we acquire
tend data for some hours to see if the two sets can be better intertwined.
I am sure Amelia can code this in a short time. In this way, they can also feed different samplings to the machine
and see if the prediction time gets shorter and shorter…

Nuria

************************************************************************************************************************

Hi Nuria

Ah sorry, I didn't know there was already a plan for XB3, I thought Xiawoei was currently making a proposal and that
we could have thrown this idea in. One or two days would probably be fine as it's just to give us an idea as to whether
or not this prediction is completely wrong. If XB3 is booked up then it will have to wait a while.
I meant to type reduced rep rate before, sorry. We don't necessarily need to reduce the power as we would still want to
operate at a where we have breakdowns regularly. It would just be nice to keep the pulse compressor stable in case it
interlocks a lot (the vacuum interlocks can leave it switched off for several seconds).

For the timing, I verified it on XB2 a few months ago with a signal generator but I haven't looked at XB3 yet so you're
right that we don't know if it's accurate. Christoph contacted Joesph Tagg and after examining the code he said that
they "should" be correct but we would probably need to measure it to be sure.

If Amelia can eventually change the code to facilitate some higher resolution data taking that would be great but I
figured that would be little more invasive and require a more detailed proposal.

Regards
Lee
************************************************************************************************************************