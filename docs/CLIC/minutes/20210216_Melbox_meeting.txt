************************************************************************************************************************
************************************************** Minutes *************************************************************
************************************************************************************************************************

2 Masterstudents will analize data on the new Xbox test stand starting from March over a period of 2 years.
Currently the test stand is built up, the goal is to have pulses by the end of the year.
Test stand is on Labview, but will eventually be in Epics.
Especially our lessons learned triggered much attention, because they do not want to have a time misalignment ether.

************************************************************************************************************************

Consecutive mail Lee about presure rise reasons:

Thanks for joining everyone(and thank you again Christoph for another nice presentation), I hope the meeting was useful.
Interesting questions! I'll try to answer but first I'd like to share a presentation (slide 22 here)
I've been meaning to mention for a while. We have colleagues at KEK in Japan and they have a cavity which they monitor
with a camera. During operation they see glowing emission sites all the time which change and occasionally one of them
goes pop. I'm not sure if they detect changes several seconds in advance but they operate CW and at much lower frequency
so it's not exactly like our setup. It's a cool experiment anyway and I wanted to highlight it just to add to the
discussion.

Now for the questions:
Would it be possible to estimate the amount of the gas released, like rise of pressure x volume of the cavity somehow?
Absolutely. Based on the volume of the system and the pressure rise detected by each pump we could estimate the amount
of gas released, we just wouldn't know what was released exactly. We did have an RGA (residual gas analyser) previously
for this but it didn't work very well.
Maybe that "amount" of gas is compatible with the process you described (cracks migrating),
maybe it's too big or too small?  This is a good point, but I'm not sure how much gas would be associated with these
processes or how to estimate (I guess it would depend on the electric field,
the surface area and what is on the surface). I'll mention it to Walter, maybe he can think of a reasonable estimate.

Would you have an idea of the time needed to warm up that gas and turn it into some plasma?
(what's the metal you use where sparks occurs? where the migration of cracks could occur?)
It is important to note that although some ionisation is necessary for breakdown, it's actually the emission site which
heats up, vaporises and causes the arc, not just the surrounding gas. It depends on how you measure it but the onset of
breakdown occurs on a nanosecond scale (high speed videos have been taken of it happening).
Though the emission sites begin to warm up and glow under normal operation so like I said,
it's difficult to say when the arc "starts".
It is also more complicated for the RF experiments with larger gaps.

Do  you have an idea of the minimum amount of plasma needed to have that spark/shortcut?
I'm not sure but this could be estimated by looking at the breakdown sites
(by looking at the amount of copper which has been removed). Walter might even know this from previous experiments.

Do you have an idea of the amount of gas a crater + plasma process would release?
(can we use the pictures of crater you showed to estimate the depth and diameter?) Same as my previous answer,
I'm not sure but it could probably be estimated and Walter might already know.

Do you think it's something "new" because you're using stronger gradient / smaller cavities?
Even maybe because you record trend data with an higher frequency compared to other experiments in the past?
This is an interesting question, we have had experiments at higher gradients and at higher frequencies
(CLIC used to be 30GHz and the components were tiny). It could be new however.
When operating these experiments nobody acquires high resolution vacuum data,
they just use the vaccum readings to ensure the pressure is low enough to operate the RF
(this is part of the reason why we didn't pay much attention to our TrendData,
we never planned to use it for detailed analysis, it was just for safety/make sure we had no leaks).
If the pressure spikes do occur first, I can see why nobody would have noticed before
(or maybe they noticed and dismissed it).

I'll bring this up next time I speak to Walter to see if he has any input/anything to add. Good questions!

************************************************************************************************************************